<?php 
    $this->load->view('header');
?>
<!-- Tampilkan data yang diperlukan dari session -->
<div class="content">
    <h2 align="center">Identitas</h2>
    <form method="post" action="<?php echo base_url(); ?>index.php/WelcomeTIKU/pesanan">
        <h3><?php echo $this->session->userdata("judul"); ?></h3>
        <p><?php $tn=$this->session->userdata("tanggal_nonton");
            echo date('l', strtotime($tn)).", ".$tn."/ ".$this->session->userdata("jadwal"); ?></p>
        <hr width="27%" align="left">
        <!-- Menampilkan kursi yang telah dipilih -->
        <ul style="list-style-type: none;">
        <?php $i=0;foreach($data['kursi'] as $kursi){?> 
            <li><h3>Kursi <?php echo $kursi;?></h3></li>
            <li>Nama :<br> <input type="text" name="nama[]" required></li>
            <li>Umur :<br><input type="text" name="umur[]" required></li>
            <li>No. KTP:<br><input type="text" name="ktp[]" required></li>
            <li><hr width="25%" style="border-top: dotted 2px;" align="left"></li>
            <?php $i++;}?>
            
        </ul>
        <hr width="27%" align="left">
        <p>Total : <?php echo number_format(count($data['kursi'])*60000,2,',','.');?></p>
        <br>
        <button type="submit" class="btn">Submit</button>
    </form> 
    <br>
    </div> <!--content-->
</body>
</html>