<?php 
    $this->load->view('header');
?>
    <h2 align="center">Edit Kursi</h2><br>
    <div class="content">
    <!-- Buat Form untuk melakukan edit, dasarnya adalah menampilkan yang sudah dipilih dan sudah dibooked -->
    <form method="post" action="<?php echo base_url(); ?>index.php/WelcomeTIKU/identitas">
        <!-- Tampilkan data yang diperlukan dari session -->
        <b>Film : </b>  
        <?php echo $this->session->userdata("judul"); ?>
        <br>
        <b>Tanggal/ Jadwal : </b>
        <?php $tn=$this->session->userdata("tanggal_nonton");
            echo date('l', strtotime($tn)).", ".$tn."/ ".$this->session->userdata("jadwal"); ?>
        <br>
        <b>Tempat Duduk : </b>
        <table id="seatsBlock">
            <tr>
                <td></td>
                <td>1</td>
                <td>2</td>
                <td>3</td>
                <td>4</td>
            </tr>
            <tr>
                <?php
                    $k=0;
                    for ($i='A'; $i<='E'; $i++){   ?>    
                        <tr>
                            <td><?php echo $i; ?></td>
                            <!-- Tampilkan kursi terpilih sebelumnya -->
                            <?php for($j=1; $j<=4; $j++){ 
                                $ij=$data['kursi'][$k]['nokur'];?>
                            <td>
                                <input name="pilihKursi[]" type="checkbox" value="<?php echo $ij; ?>" 
                                <?php foreach($data['kursi_checked'] as $kursi){ 
                                    if($ij==$kursi)
                                        echo "checked";
                                }
                                //menampilkan kursi yang sudah dibooked orang lain
                                for ($x=0; $x<count($data['kursi_booked']); $x++){
                                    if($ij==$data['kursi_booked'][$x]['nokur'])
                                        echo "disabled";
                                }
                                ?>
                                <?php ?>
                                >
                            </td>
                            <?php $k++;} ?>
                        </tr>
                    <?php } ?>
            </tr>                
        </table><br>Harga Rp 60.000,00/ tiket<br><br><button class="btn" type="submit">Submit</button>
    </form>
    </div> <!--content-->
    </body>
</html>