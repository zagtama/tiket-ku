<?php 
    $this->load->view('header');
?>
<!-- Tampilkan data yang diperlukan dari session -->
<div class="content">
    <h2 align="center">Pesanan Tiket</h2>
    <h3><?php echo $this->session->userdata("judul"); ?></h3>
    <p><?php $tn=$this->session->userdata("tanggal_nonton");
        echo date('l', strtotime($tn)).", ".$tn."/ ".$this->session->userdata("jadwal"); ?></p>
    <hr width="25%" align="left">
    <!-- Menampilkan kursi yang telah dipilih -->
    <ul style="list-style-type: none;">
        <?php $i=0;foreach($data['kursi'] as $kursi){?> 
        <li>Nama : <?php echo $data['nama'][$i]; ?> </li>
        <li>Umur : <?php echo $data['umur'][$i]; ?> </li>
        <li>KTP&ensp; : <?php echo $data['ktp'][$i]; ?></li>
        <li>
            <form method="post" action="<?php echo base_url()."index.php/WelcomeTIKU/hapusKursi/$i"; ?>">
                <b>Kursi <?php echo $kursi;?></b>
                <button type="submit" name="submit" class="btn hapus">Hapus </button>   
            </form>
        </li>
        <li><hr width="23%" style="border-top: dotted 2px;" align="left"></li>
        <?php $i++;}?>
        
    </ul>
    <hr width="25%" align="left">
    <p>Total : <?php echo number_format(count($data['kursi'])*60000,2,',','.');?></p>
    <hr width="25%" align="left">
    <!-- Pilihan Edit Kursi -->
    <form method="post" action="<?php echo base_url()."index.php/WelcomeTIKU/edit"; ?>">
        <button type="submit" name="submit" class="btn edit">Edit Kursi </button>
    </form>
    <!-- Pilihan Hapus Semua Kursi -->
    <form method="post" action="<?php echo base_url(); ?>">
        <button type="submit" name="submit" class="btn hapus">Hapus Semua</button>
    </form>
    <br>
    <!-- Pilihan Bayar -->
    <form method="post" action="<?php echo base_url(); ?>index.php/WelcomeTIKU/bayar">
        <button type="submit" class="btn">Bayar</button>
    </form> 
    <br>
    </div> <!--content-->
</body>
</html>